const Model = require(mvp_mrc_demo-main/models/Model.js)

class Post extends Model {
  constructor() {
    super();
    this.database = require("../db/posts.json");
  }

  getAllPosts() {
    return super.getAllData();
  }

  getPostById(id) {
    return super.getById(id);
  }
}


module.exports = Post;
