const Model = require(mvp_mrc_demo-main/models/Model.js)

class User extends Model {
  constructor() {
    super();
    this.database = require("../db/users.json");
  }

  getAllUser() {
    return super.getAllData();
  }

  getUserById(id) {
    return super.getById(id);
  }
}


module.exports = User;
